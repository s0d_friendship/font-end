# base image
FROM node:12

# set working directory
WORKDIR /app

# install and cache app dependencies
COPY package.json .
RUN npm install
RUN npm install -g @angular/cli

COPY . .

RUN npm run build
#RUN node --max-old-space-size=8192 ./node_modules/@angular/cli/bin/ng build --aot=true --optimization=true --source-map=false

EXPOSE 3000

CMD [ "node", "server.js" ]