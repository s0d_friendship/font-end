/**
 * Created by HoangNH on 3/22/2017.
 */
import { Injectable, ViewContainerRef, Type, ComponentFactoryResolver } from '@angular/core';

@Injectable()
export class HelperService {

    constructor(private _componentFactoryResolver: ComponentFactoryResolver) {
    }

    createComponent(viewContainerRef: ViewContainerRef, component: Type<any>) {
        const componentFactory = this._componentFactoryResolver.resolveComponentFactory(component);
        const componentRef = viewContainerRef.createComponent(componentFactory);
        return componentRef.instance;
    }

    openPrintWindow(title: string, content: string, style?: string) {
        const htmlContent = ` <!DOCTYPE html>
                    <html>
                    <head>
                        <title>${title}</title>
                        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN"
                            crossorigin="anonymous">
                        <style>
                        body{
                        color: white;
                        }
                            @page {
        size: auto;
        margin: 0mm 0;
    }
    @media print {
        * {
            margin: 0;
            padding: 0;
            font-size: 11px;
            box-sizing: border-box;
        }
        html,
        body {
            color: black;
            width: 100%;
            height: 100%;
            padding: 0;
            margin: 0;
        }
        header {
            padding-top: 10px;
        }
        header,
        footer {
            text-align: center;
        }
        header img {
            width: 70%;
        }
        footer img {
            width: 100%;
        }
        .print-barcode-page{
        margin-top: 0; margin-bottom: 0;
        padding-top: -5;
        padding-bottom: 0;
        margin-left: 0px !important;
        display: block;
        width: 100%;
        }
        .print-barcode-page .barcode-item{
            padding-left: 0px !important;
            padding-right: 0 !important;
            padding-top: 0 !important;
            padding-bottom: 5px !important;
            overflow: hidden;
            width: 50%;
            display: inline-block;
            float: left;
        }
        .print-barcode-page .barcode-item .barcode-wrapper{
        display: block;
        width: 100%;
        text-align: center !important;
        }
        .print-barcode-page .barcode-item img{
        display: block;
        width: 100%;
        }
        .print-page {
            width: 100%;
            height: 100%;
            position: relative;
            padding: 20px 20px;
        }
        .print-page footer {
            position: absolute;
            bottom: 0;
            left: 0;
            right: 0;
        }
        div.wrapper-table {
            padding: 0 30px;
            text-align: center;
        }
        table.bordered {
            border: 1px solid black;
            width: 100%;
            max-width: 100%;
            margin-bottom: 1rem;
            border-collapse: collapse;
            background-color: transparent;
            margin-top: 20px;
        }
        table.bordered thead tr th,
        table.bordered tbody tr td {
            border: 1px solid black;
            font-size: 12px !important;
            text-align: center;
            padding: 3px;
        }
        table.bordered tbody tr td a {
            text-decoration: none;
            text-align: left;
            font-size: 14px;
        }
        .middle {
            vertical-align: middle;
        }
        .pr-w-30 {
            width: 30px !important;
        }
        .pr-w-27 {
            width: 27px !important;
        }
        .pr-w-70 {
            width: 70px !important;
            min-width: 70px !important;
            max-width: 70px !important;
        }
        .pr-w-100 {
            width: 100px !important;
        }
        .pr-w-55 {
            width: 55px !important;
            min-width: 55px !important;
            max-width: 55px !important;
        }
        .pd-5{
        padding: 5px !important;
        }
        .pdr-5{
        padding-right: 5px !important;
        }
        .pdl-5{
        padding-left: 5px !important;
        }
        .pd-10{
        padding: 10px !important;
        }
        .w70 {
        width: 70px !important;
        }
        .w50{
        wdith: 50px !important;
        }
        .w150 {
            width: 150px !important;
        }
        .w250{
        width: 250px !important;
        }
        .center {
            text-align: center;
        }
        .pr-va-top {
            vertical-align: top !important;
        }
        .page-break {
            page-break-after: always;
            border-top: 1px solid transparent;
            margin: 1px;
        }
        .visible-print {
            display: block;
        }
        .hidden-print {
            display: none;
        }
        .text-left {
            text-align: left !important;
        }
        .text-right {
            text-align: right !important;
        }
        .w100pc {
            width: 100%;
        }
        .uppercase {
            text-transform: uppercase;
        }
        table .dotted-control {
            border-bottom: 1px dotted black;
            text-align: left;
        }

        table td {
            padding-top: 3px;
            padding-bottom: 3px;
        }

        table td div.control-group {
            display: table;
            width: 100%;
        }

        table td div.control-group label {
            width: 1%;
            white-space: nowrap;
        }

        table td div.control-group label,
        table td div.control-group div {
            display: table-cell;
        }
        ${style}
    }
                        </style>
                     </head>
                     <body onload="window.print();window.close()">
                     ${content}
                     </body>
                     </html>
        `;
        let popupWin;
        const dualScreenLeft = window.screenLeft !== undefined ? window.screenLeft : 0;
        const dualScreenTop = window.screenTop !== undefined ? window.screenTop : 0;
        const width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        const height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;
        const w = window.outerWidth;
        const h = window.outerHeight;
        const left = ((width / 2) - (w / 2)) + dualScreenLeft;
        const top = ((height / 2) - (h / 2)) + dualScreenTop;
        popupWin = window.open('', '_blank', 'width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
        popupWin.document.open();
        popupWin.document.write(htmlContent);
        popupWin.document.close();
    }
}
