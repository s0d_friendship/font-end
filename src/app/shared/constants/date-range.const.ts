export const DateRange = {
    today: 0,
    recentOneWeek: 1,
    thisWeek: 2,
    lastWeek: 3,
    recentOneMonth: 4,
    thisMonth: 5,
    lastMonth: 6,
    recentQuarter: 7,
    thisQuarter: 8,
    lastQuarter: 9,
    recentYear: 10,
    thisYear: 11,
    lastYear: 12,
    all: 13
};
