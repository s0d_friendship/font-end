export enum TaskStatus {
    notStartYet,
    inProgress,
    completed,
    canceled,
    pendingToFinish,
    declineFinish
}

export enum TaskType {
    combination = 0,
    personal = 1,
}

export enum MethodCalculatorResult {
    checkList = 0,
    manually = 1,
    byChild = 2,
}

export  enum TaskWorkingType {
    personal,
    combination,
    both,
    create,
}
