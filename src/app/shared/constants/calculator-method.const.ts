export const CalculatorMethod = {
    specificIdentificationMethod: 0,
    weightedAverage: 1,
    weightedAverageImmediately: 2,
    firstInFirstOut: 3,
    lastInFirstOut: 4,
};
