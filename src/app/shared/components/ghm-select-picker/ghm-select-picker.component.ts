import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewEncapsulation} from '@angular/core';
import {GhmSelectPickerModel} from './ghm-select-picker.model';
import * as _ from 'lodash';
import {UtilService} from '../../services/util.service';

@Component({
    selector: 'ghm-select-picker',
    templateUrl: 'ghm-select-picker.component.html',
    styleUrls: ['./ghm-select-picker.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class GhmSelectPickerComponent implements OnInit, OnChanges {
    @Input() isShow = false;
    @Input() allTitle = '';
    @Input() selectedTitle = '';
    @Input() source = [];
    @Input() selectedItems = [];
    @Input() paging = false;
    @Input() totalRows = 0;
    @Input() pageSize = 0;
    @Input() title = '';
    @Input() isTreeView = false;

    @Output() selectedItem = new EventEmitter();
    @Output() selectedPage = new EventEmitter();
    @Output() removedItem = new EventEmitter();
    @Output() accepted = new EventEmitter();

    errorMessage = '';
    listPages = [];
    currentPage = 1;
    listSource: GhmSelectPickerModel[] = [];
    keyword = '';

    constructor(private utilService: UtilService) {
    }

    ngOnInit() {
        this.listSource = this.source;
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.hasOwnProperty('totalRows') && changes['totalRows'].currentValue !== 0) {
            this.renderPaging();
        }
    }

    show(source?: any) {
        this.isShow = true;
        this.keyword = '';
        this.listSource = source;
    }

    dismiss() {
        this.isShow = false;
    }

    pageClick(pageNumber: number) {
        if (this.paging) {
            this.currentPage = pageNumber;
            this.selectedPage.emit();
        }
    }

    selectItem(item: GhmSelectPickerModel) {
        this.errorMessage = '';
        this.selectedItem.emit(item);
        const existingItem = _.find(this.selectedItems, (selectedItem: GhmSelectPickerModel) => {
            return selectedItem.id === item.id;
        });
        if (existingItem) {
            return;
        }
        this.selectedItems.push(item);
    }

    removeItem(id: any) {
        _.remove(this.selectedItems, (selectedItem: GhmSelectPickerModel) => {
            return selectedItem.id === id;
        });
        this.removedItem.emit(id);
    }

    accept() {
        if (!this.selectedItems || this.selectedItems.length === 0) {
            this.errorMessage = 'required';
            return;
        }
        this.accepted.emit(this.selectedItems);
        this.isShow = false;
        this.selectedItems = [];
    }

    search() {
        if (this.keyword && this.keyword.trim()) {
            this.listSource = _.filter(this.source, (item: GhmSelectPickerModel) => {
                return this.stripToVietnameChar(item.name).indexOf(this.stripToVietnameChar(this.keyword)) > -1;
            });
        } else {
            this.listSource = this.source;
        }
    }

    private renderPaging() {
        if (this.paging) {
            const totalPage = Math.ceil(this.totalRows / this.pageSize);
            for (let i = 1; i <= totalPage; i++) {
                this.listPages.push(i);
            }
        }
    }

    private stripToVietnameChar(str): string {
        str = str.toLowerCase();
        str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
        str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
        str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
        str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
        str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
        str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
        str = str.replace(/đ/g, 'd');
        return str;
    }
}
