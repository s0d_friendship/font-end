export interface GhmSelectPickerModel<T = any> {
    id: any;
    parentId?: number;
    name: string;
    data?: T;
}
