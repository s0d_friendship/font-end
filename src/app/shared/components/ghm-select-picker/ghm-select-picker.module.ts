import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GhmSelectPickerComponent } from './ghm-select-picker.component';
import { GhmSelectPickerService } from './ghm-select-picker.service';
import {GhmInputModule} from '../ghm-input/ghm-input.module';
import {FormsModule} from '@angular/forms';
import {DxTreeViewModule} from 'devextreme-angular';

@NgModule({
    imports: [CommonModule, GhmInputModule, FormsModule, DxTreeViewModule],
    exports: [GhmSelectPickerComponent],
    declarations: [GhmSelectPickerComponent],
    providers: [GhmSelectPickerService],
})
export class GhmSelectPickerModule {
}
