import { ElementRef, Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class NhModalService {
    dismiss$ = new Subject();

    private listModals: ElementRef[] = [];

    constructor() {
    }

    get countActiveModal(): number {
        return this.listModals.length;
    }

    add(nhModalComponent: ElementRef) {
        this.listModals.push(nhModalComponent);
    }

    dismiss() {
        this.dismiss$.next();
    }

    remove(el: ElementRef) {
        const index = this.listModals.indexOf(el);
        this.listModals.splice(index, 1);
    }
}
