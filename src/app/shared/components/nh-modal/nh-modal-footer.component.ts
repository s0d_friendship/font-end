/**
 * Created by HoangNH on 5/5/2017.
 */
import {
    AfterViewInit, Component, enableProdMode, OnInit} from '@angular/core';

if (!/localhost/.test(document.location.host)) {
    enableProdMode();
}
@Component({
    selector: 'nh-modal-footer',
    template: `
        <ng-content></ng-content>
    `
})
export class NhModalFooterComponent implements OnInit, AfterViewInit {
    constructor() {

    }

    ngOnInit() {

    }

    ngAfterViewInit() {

    }
}
