/**
 * Created by HoangNH on 5/5/2017.
 */
import {
    Component,
    ContentChild,
    ContentChildren,
    ElementRef, enableProdMode,
    EventEmitter,
    HostListener,
    Input,
    OnInit,
    Optional,
    Output,
    QueryList,
    Renderer2,
    SkipSelf,
    TemplateRef,
    ViewChild, ViewChildren,
    ViewContainerRef,
    ViewEncapsulation
} from '@angular/core';
import {NhModalHeaderComponent} from './nh-modal-header.component';
import {NhModalFooterComponent} from './nh-modal-footer.component';
import {NhModalService} from './nh-modal.service';
import {animate, AnimationEvent, state, style, transition, trigger} from '@angular/animations';
import {GlobalPositionStrategy, Overlay, OverlayRef} from '@angular/cdk/overlay';
import {TemplatePortal} from '@angular/cdk/portal';

if (!/localhost/.test(document.location.host)) {
    enableProdMode();
}
@Component({
    selector: 'nh-modal',
    template: `
        <ng-template #contentRef>
            <!--<div class="nh-modal-container">-->
            <!--<div class="nh-modal-backdrop" *ngIf="hasBackdrop" [@fadeInOut]="isOpen ? 'in' : 'out'"-->
            <!--(click)="backdropClick($event)"></div>-->
            <div [ngClass]="'nh-modal-wrapper nh-modal-' + size" #modalDialog
                 [@slideUpDown]="isOpen ? 'show' : 'hide'"
                 (@slideUpDown.start)="onAnimationEvent($event)"
                 (@slideUpDown.done)="onAnimationEvent($event)">
                <ng-content></ng-content>
            </div>
            <!--</div>&lt;!&ndash; END: nh-modal-container &ndash;&gt;-->
        </ng-template>
    `,
    styleUrls: ['./nh-modal.component.scss'],
    encapsulation: ViewEncapsulation.None,
    providers: [NhModalService],
    animations: [
        trigger('slideUpDown', [
            state('hide', style({
                display: 'none',
                transform: 'translateY(-110%)'
            })),
            state('show', style({
                display: 'block',
                transform: 'translateY(0)'
            })),
            transition('hide => show', [
                animate('300ms ease-out')
            ]),
            transition('show => hide', [
                animate('300ms ease-in')
            ])
        ])
    ]
})
export class NhModalComponent implements OnInit {
    @ContentChild(NhModalHeaderComponent , {static: true}) modalHeaderComponents: NhModalHeaderComponent;
    @ContentChild(NhModalFooterComponent , {static: true}) modalFooterComponents: NhModalFooterComponent;
    @ContentChildren(NhModalComponent) modelComponents: QueryList<NhModalComponent>;
    @ViewChild('contentRef' , {static: true}) contentRef: TemplateRef<any>;
    @ViewChild('modalDialog' , {static: true}) modalDialog: ElementRef;

    @Input() effect = 'slideDown'; // zoom, fade, slideUp, slideDown, slideLeft, slideRight
    @Input() size = 'sm';
    @Input() hasBackdrop = true;
    @Input() backdropStatic = true;

    // -- Hàm không sử dụng.
    @Output() onShow = new EventEmitter();
    @Output() onShown = new EventEmitter();
    @Output() onHide = new EventEmitter();
    @Output() onHidden = new EventEmitter();
    @Output() onClose = new EventEmitter();
    // END: -- Hàm không sử dụng.

    @Output() show = new EventEmitter();
    @Output() shown = new EventEmitter();
    @Output() hide = new EventEmitter();
    @Output() hidden = new EventEmitter();
    @Output() close = new EventEmitter();

    isOpen = false;
    state = '';
    hideState = ['zoomOut', 'fadeOut', 'slideUpHide', 'slideDownHide', 'slideLeftHide', 'slideRightHide'];
    showState = '';
    isDone = false;
    private overlayRef: OverlayRef;
    private positionStrategy = new GlobalPositionStrategy();

    constructor(private el: ElementRef,
                private renderer: Renderer2,
                private nhModalService: NhModalService,
                private overlay: Overlay,
                private viewContainerRef: ViewContainerRef,
                @SkipSelf() @Optional() private nhModuleModalService: NhModalService) {
        // this.changeState(true);
        this.nhModalService.dismiss$.subscribe(result => {
            this.close.emit();
            this.dismiss();
        });
    }

    @HostListener('window:resize', ['$event'])
    onWindowResize() {
        this.updatePositionStrategy();
    }

    @HostListener('window:keyup', ['$event'])
    onWindowKeyup(event: KeyboardEvent) {
        // Escape press: Close modal.
        if (event.key === 'Escape') {
            this.dismiss();
        }
    }

    ngOnInit() {
        this.overlayRef = this.overlay.create({
            positionStrategy: this.positionStrategy,
            hasBackdrop: this.hasBackdrop
        });
        this.overlayRef.backdropClick().subscribe(event => {
            if (!this.backdropStatic) {
                this.dismiss();
            }
        });
    }

    onAnimationEvent(event: AnimationEvent) {
        if (event.phaseName === 'start') {
            if (event.fromState === 'show' && event.toState === 'hide') {
                this.hide.emit();
            } else {
                this.shown.emit();
                this.updatePositionStrategy();
            }
        } else {
            if (event.fromState === 'show' && event.toState === 'hide') {
                this.renderer.removeClass(this.el.nativeElement, 'open');
                this.nhModuleModalService.remove(this.el);
                if (this.nhModuleModalService.countActiveModal === 0) {
                    this.renderer.removeClass(document.body, 'nh-modal-open');
                }
                this.hidden.emit();
                this.detachOverlay();
            } else {
                this.shown.emit();
            }
        }
    }

    open() {
        if (!this.overlayRef.hasAttached()) {
            this.overlayRef.attach(new TemplatePortal(this.contentRef, this.viewContainerRef));
            this.isOpen = true;
        }
        this.renderer.addClass(document.body, 'nh-modal-open');
        this.nhModuleModalService.add(this.el);
    }

    dismiss() {
        this.isOpen = false;
    }

    private detachOverlay() {
        if (this.overlayRef && this.overlayRef.hasAttached()) {
            setTimeout(() => {
                this.overlayRef.detach();
            });
        }
    }

    private updatePositionStrategy() {
        if (this.overlayRef.hasAttached()) {
            this.setModalWidth();
            const windowWidth = window.innerWidth;
            const overlayWidth = this.overlayRef.overlayElement.getElementsByClassName('nh-modal-wrapper')[0].clientWidth;
            const left = (windowWidth - this.setModalWidth()) / 2;
            console.log(this.el.nativeElement.offsetWidth);
            this.positionStrategy.top('50px');
            this.positionStrategy.left(`${Math.round(left)}px`);
            this.positionStrategy.apply();
        }
    }

    private setModalWidth() {
        switch (this.size) {
            case 'sm':
                return 300;
            case 'md':
                return 600;
            case 'lg':
                return 1000;
            default:
               break;
                // this.renderer.setStyle(this.modalDialog.nativeElement, 'width', `${window.innerWidth - 50}px`);
        }
    }
}
