import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {GhmAppSuggestionComponent} from './ghm-app-suggestion.component';
import {CoreModule} from '../../../core/core.module';

@NgModule({
    declarations: [GhmAppSuggestionComponent],
    imports: [
        CommonModule, CoreModule
    ],
    exports: [GhmAppSuggestionComponent]
})
export class GhmAppSuggestionModule {
}
