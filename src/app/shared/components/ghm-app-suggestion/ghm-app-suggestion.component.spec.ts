import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GhmAppSuggestionComponent } from './ghm-app-suggestion.component';

describe('GhmAppSuggestionComponent', () => {
  let component: GhmAppSuggestionComponent;
  let fixture: ComponentFixture<GhmAppSuggestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GhmAppSuggestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GhmAppSuggestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
