import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
    selector: 'ghm-app-suggestion',
    templateUrl: './ghm-app-suggestion.component.html',
    styleUrls: ['./ghm-app-suggestion.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class GhmAppSuggestionComponent implements OnInit {
    @Input() header = 'Bạn đang xem mục tiêu của';
    @Input() avatar = '';
    @Input() title = '';
    @Input() description = '';

    constructor() {
    }

    ngOnInit() {
    }

}
