import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import * as _ from 'lodash';
import {Checklist} from './ghm-checklist.model';
import {UtilService} from '../../../services/util.service';

@Component({
    selector: 'app-ghm-checklist',
    templateUrl: './ghm-checklist.component.html',
    styleUrls: ['./ghm-checklist.component.scss']
})
export class GhmChecklistComponent implements OnInit {
    // @ViewChild('ghmInput') ghmInputElement: GhmInputComponent;
    @Input() source = [];
    @Input() allowAdd = true;
    @Input() allowComplete = true;
    @Input() allowEdit = true;
    @Input() allowDelete = true;
    @Input() buttonText = 'Thêm checklist';

    @Output() removed = new EventEmitter();
    @Output() saved = new EventEmitter();
    @Output() completeChange = new EventEmitter();

    checklist: Checklist;
    errorMessage = '';
    checkListName;
    isViewMore;

    private _isShowForm = false;

    constructor(private utilService: UtilService) {
    }

    get isShowForm() {
        return this._isShowForm;
    }

    set isShowForm(value: any) {
        this._isShowForm = value;
        if (value) {
            this.checklist = new Checklist();
        }
        setTimeout(() => {
            this.utilService.focusElement('ghmInput');
        });
    }

    ngOnInit() {
    }

    onCompleteChange(checklist: Checklist) {
        checklist.isComplete = !checklist.isComplete;
        this.completeChange.emit(checklist);
    }

    edit(checklist: Checklist) {
        if (!this.allowEdit) {
            return;
        }

        this.checkListName = checklist.name;
        _.each(this.source, (item) => {
            item.isEdit = false;
        });

        this.isShowForm = false;

        checklist.isEdit = true;
        this.checklist = checklist;
    }

    remove(id: any) {
        _.remove(this.source, (checklist: Checklist) => {
            return checklist.id === id;
        });
        this.removed.emit(id);
    }

    save() {
        if (!this.checklist.name) {
            this.errorMessage = 'Vui lòng nhập checklist';
            return;
        }
        const isExists = _.find(this.source, (checklist: Checklist) => {
            return checklist.name === this.checklist.name && checklist.id !== this.checklist.id;
        });
        if (isExists) {
            this.errorMessage = 'Checklist đã tồn tại';
            return;
        }
        this.saved.emit(this.checklist);
        if (this.checklist.id) {
            const checklistInfo = _.find(this.source, (checklist: Checklist) => {
                return checklist.id === this.checklist.id;
            });
            if (checklistInfo) {
                checklistInfo.name = this.checklist.name;
                checklistInfo.isEdit = false;
            }
        }
        this.resetModel();
    }

    closeForm(checkList?: Checklist) {
        if (checkList) {
            checkList.isEdit = false;
            checkList.name = this.checkListName;
        } else {
            this.isShowForm = false;
        }

        this.errorMessage = '';
    }

    addCheckList() {
        this.isShowForm = true;
        this.utilService.focusElement('ghmInputAdd');
    }

    private resetModel() {
        this.checklist = new Checklist();
        this.errorMessage = '';
        this.isShowForm = false;
    }
}
