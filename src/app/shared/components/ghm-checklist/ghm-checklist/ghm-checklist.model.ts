export class Checklist {
    id: any;
    name: string;
    concurrencyStamp?: string;
    isComplete?: boolean;
    isEdit?: boolean;

    constructor() {
        this.isComplete = false;
        this.isEdit = false;
        this.name = '';
        this.id = null;
        this.concurrencyStamp = null;
    }
}
