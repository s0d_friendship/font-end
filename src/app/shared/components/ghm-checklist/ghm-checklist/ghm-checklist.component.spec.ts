import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GhmChecklistComponent } from './ghm-checklist.component';

describe('GhmChecklistComponent', () => {
  let component: GhmChecklistComponent;
  let fixture: ComponentFixture<GhmChecklistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GhmChecklistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GhmChecklistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
