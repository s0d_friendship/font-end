import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GhmChecklistComponent } from './ghm-checklist/ghm-checklist.component';
import { MatCheckboxModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { GhmInputModule } from '../ghm-input/ghm-input.module';

@NgModule({
    declarations: [GhmChecklistComponent],
    imports: [
        CommonModule, MatCheckboxModule, FormsModule, GhmInputModule
    ],
    exports: [GhmChecklistComponent]
})
export class GhmChecklistModule {
}
