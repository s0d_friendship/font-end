import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ExplorerItem} from '../ghm-file-explorer/explorer-item.model';
import * as _ from 'lodash';
import {ViewType} from '../../constants/view-type.const';
import {GhmAttachmentService} from './ghm-attachment.service';
import {GhmAttachment} from './ghm-attachment.model';
import * as FileSaver from 'file-saver';
import {environment} from '../../../../environments/environment';
import {DocumentTypeConst} from '../ghm-document-viewer/ghm-document-viewer/document-tyle.const';
import {GhmDocumentViewerComponent} from '../ghm-document-viewer/ghm-document-viewer/ghm-document-viewer.component';
import {FileSearchViewModel} from '../../../modules/folders/viewmodels/file-search.viewmodel';
import {MatDialog} from '@angular/material';

@Component({
    selector: 'ghm-attachments',
    templateUrl: './ghm-attachments.component.html',
    styleUrls: ['./ghm-attachments.component.scss'],
    providers: [GhmAttachmentService]
})

export class GhmAttachmentsComponent implements OnInit {
    @Input() isMultiple = true;
    @Input() isReadOnly = false;
    @Input() isShowTitle = true;
    @Input() objectId;
    @Input() objectType;
    @Input() isEnable = true;
    @Input() listFileAttachment = [];
    @Output() selectFile = new EventEmitter(); // Có 2 trường hợp emit 1 file fileInsert và nhiêu file listFileInsert
    @Output() removeFile = new EventEmitter();
    viewType = ViewType;
    type = ViewType.list;
    urlFile = `${environment.fileUrl}`;

    constructor(private ghmAttachmentService: GhmAttachmentService,
                private dialog: MatDialog) {
    }

    ngOnInit() {
    }

    acceptSelectedFile(files: ExplorerItem[]) {
        if (files && files.length > 0) {
            const listAttachmentInsert: GhmAttachment[] = [];
            _.each(files, (file: ExplorerItem) => {
                const fileInfo = _.find(this.listFileAttachment, (attachment: GhmAttachment) => {
                    return attachment.fileId === file.id;
                });

                if (!fileInfo) {
                    const ghmAttachment = new GhmAttachment('', this.objectId, this.objectType, file.id.toString(), file.name,
                        file.type, file.url, file.createTime, file.creatorFullName, file.creatorId, file.extension, file.isImage);
                    listAttachmentInsert.push(ghmAttachment);
                }
            });

            this.selectFile.emit({listFileInsert: listAttachmentInsert});
        }
    }

    selectItem(file: ExplorerItem) {
        const fileInfo = _.find(this.listFileAttachment, (attachment: GhmAttachment) => {
            return attachment.fileId === file.id;
        });

        if (!fileInfo) {
            const attachment = new GhmAttachment('', this.objectId, this.objectType, file.id.toString(), file.name,
                file.type, file.url, file.createTime, file.creatorFullName, file.creatorId, file.extension, file.isImage);

            this.selectFile.emit({fileInsert: attachment});
        }
    }

    delete(item: GhmAttachment) {
        _.remove(this.listFileAttachment, (attachment: GhmAttachment) => {
            return attachment.fileId === item.fileId;
        });
        this.removeFile.emit(item);
    }

    downloadFile(attachment: GhmAttachment) {
        this.ghmAttachmentService.download(attachment.fileId).subscribe(
            result => {
                FileSaver.saveAs(result, `${attachment.fileName}.${attachment.extension}`);
                const fileURL = URL.createObjectURL(result);
            });
    }

    checkIsImage(extension: string) {
        return ['png', 'jpg', 'jpeg', 'gif'].indexOf(extension) > -1;
    }

    showFile(item: GhmAttachment) {
        const type = item.extension === 'pdf' ? DocumentTypeConst.pdf : DocumentTypeConst.docx;
        const dialogDơcumentViewer = this.dialog.open(GhmDocumentViewerComponent, {
            data: {type: type, fileId: item.id, url: `${this.urlFile}/${item.url}`, fileName: item.fileName, extension: item.extension},
            id: `dialog-document-viewer-${item.id}`
        });

        dialogDơcumentViewer.afterClosed().subscribe((data) => {
        });
    }
}
