import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GhmAttachmentsComponent } from './ghm-attachments.component';

describe('GhmAttachmentsComponent', () => {
  let component: GhmAttachmentsComponent;
  let fixture: ComponentFixture<GhmAttachmentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GhmAttachmentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GhmAttachmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
