import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {GhmAttachmentsComponent} from './ghm-attachments.component';
import {GhmFileExplorerModule} from '../ghm-file-explorer/ghm-file-explorer.module';
import {DatetimeFormatModule} from '../../pipe/datetime-format/datetime-format.module';
import {CoreModule} from '../../../core/core.module';
import {NhImageViewerModule} from '../nh-image-viewer/nh-image-viewer.module';
import {SweetAlert2Module} from '@sweetalert2/ngx-sweetalert2';
import {GhmDocumentViewerModule} from '../ghm-document-viewer/ghm-document-viewer.module';
import {GhmDocumentViewerComponent} from '../ghm-document-viewer/ghm-document-viewer/ghm-document-viewer.component';

@NgModule({
    imports: [
        CommonModule, CoreModule, GhmFileExplorerModule, DatetimeFormatModule, NhImageViewerModule,
        GhmDocumentViewerModule,
        SweetAlert2Module.forRoot({
            buttonsStyling: false,
            customClass: 'modal-content',
            confirmButtonClass: 'btn blue cm-mgr-5',
            cancelButtonClass: 'btn',
            showCancelButton: true,
        })
    ],
    declarations: [GhmAttachmentsComponent],
    exports: [GhmAttachmentsComponent],
    entryComponents: [GhmDocumentViewerComponent]
})
export class GhmAttachmentsModule {
}
