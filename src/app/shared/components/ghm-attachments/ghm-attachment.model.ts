export class GhmAttachment {
    id: string;
    objectId: number;
    objectType: number;
    fileId: string;
    fileName: string;
    type: string;
    url: string;
    createTime: string;
    creatorFullName: string;
    creatorId: string;
    extension?: string;
    isImage: boolean;

    public constructor(id?: string, objectId?: number, objectType?: number, fileId?: string, fileName?: string,
                       type?: string, url?: string, createTime?: string, creatorFullName?: string, creatorId?: string,
                       extension?: string, isImage?: boolean) {
        this.id = id;
        this.objectId = objectId;
        this.objectType = objectType;
        this.fileId = fileId;
        this.fileName = fileName;
        this.type = type;
        this.url = url;
        this.createTime = createTime;
        this.creatorFullName = creatorFullName;
        this.creatorId = creatorId;
        this.extension = extension;
        this.isImage = this.checkIsImage(extension);
    }

    checkIsImage(extension: string) {
        return ['png', 'jpg', 'jpeg', 'gif'].indexOf(extension) > -1;
    }
}
