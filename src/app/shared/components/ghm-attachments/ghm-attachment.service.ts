import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {environment} from '../../../../environments/environment';

@Injectable()
export class GhmAttachmentService {
    url = `${environment.fileUrl}api/v1/files`;

    constructor(private http: HttpClient) {
    }

    download(id: string) {
        return this.http.post(`${this.url}/download/${id}`, {},
            {responseType: 'blob'})
            .pipe(map(response => {
                return new Blob([response]);
            }));
    }
}
