import {
    AfterContentInit, AfterViewInit,
    Component,
    ContentChildren, ElementRef, enableProdMode, EventEmitter,
    Input,
    OnDestroy,
    OnInit, Output,
    QueryList, Renderer2, ViewChild,
    ViewEncapsulation
} from '@angular/core';
import { NhStepComponent } from './nh-step.component';
if (!/localhost/.test(document.location.host)) {
    enableProdMode();
}
@Component({
    selector: 'nh-wizard',
    templateUrl: './nh-wizard.component.html',
    styleUrls: ['./nh-wizard.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class NhWizardComponent implements OnInit, AfterContentInit, AfterViewInit, OnDestroy {
    @ContentChildren(NhStepComponent) nhStepComponents: QueryList<NhStepComponent>;
    @ViewChild('activeLine' , {static: true}) activeLine: ElementRef;
    @ViewChild('stepHeaderContainer' , {static: true}) stepHeaderContainer: ElementRef;
    @Input() currentStep = 1;
    @Input() isFinish = false;
    @Input() nextLabel = 'Tiếp theo';
    @Input() backLabel = 'Trờ về';
    @Input() finishLabel = 'Kêt thúc';
    private _allowNext = false;
    subscribers: any = {};
    isLast = false;
    steps = [];

    @Input()
    set allowNext(value) {
        this._allowNext = value;
    }

    get allowNext() {
        return this._allowNext;
    }

    @Output() stepClicked = new EventEmitter();

    constructor(private renderer: Renderer2) {
    }

    ngOnInit() {
    }

    ngAfterViewInit() {
        this.updateActiveLinePosition();
    }

    ngAfterContentInit() {
        this.steps = [];
        this.nhStepComponents.forEach((stepComponent: NhStepComponent, index: number) => {
            // Render list step header
            stepComponent.nextLabel = this.nextLabel;
            stepComponent.backLabel = this.backLabel;
            stepComponent.finishLabel = this.finishLabel;
            this.steps.push({
                id: stepComponent.step, title: stepComponent.title,
                description: stepComponent.description,
                icon: stepComponent.icon
            });
            this.updateShowStatus();
            this.subscribers.next = stepComponent.next.subscribe(() => {
                if (this.allowNext) {
                    this.next();
                }
            });
            this.subscribers.back = stepComponent.back.subscribe(() => {
                this.back();
            });
            this.subscribers.finish = stepComponent.finish.subscribe(() => {
            });
            if (index === this.nhStepComponents.length - 1) {
                stepComponent.isLast = true;
            }
        });
    }

    ngOnDestroy() {
        this.subscribers.next.unsubscribe();
        this.subscribers.back.unsubscribe();
        this.subscribers.finish.unsubscribe();
    }

    onMouseEnterHeaderStep(event: MouseEvent) {
        const element = event.target as any;
        const boundingClientRect = element.getBoundingClientRect();
        this.setActiveLinePosition(element.offsetLeft, boundingClientRect.width);
    }

    onMouseLeaveHeaderStep() {
        this.updateActiveLinePosition();
    }

    stepClick(step: any) {
        this.stepClicked.emit(step);
        this.checkLastStep();
        this.updateShowStatus();
    }

    next() {
        this.currentStep = this.currentStep + 1;
        this.checkLastStep();
        this.updateShowStatus();
        this.updateActiveLinePosition();
    }

    back() {
        if (this.currentStep === 1) {
            return;
        }

        this.currentStep = this.currentStep - 1;
        this.checkLastStep();
        this.updateShowStatus();
        this.updateActiveLinePosition();
    }

    goTo(step: number) {
        this.currentStep = step;
    }

    private checkLastStep() {
        this.isLast = this.nhStepComponents.length === this.currentStep;
    }

    private updateShowStatus() {
        this.nhStepComponents.forEach((stepComponent) => {
            stepComponent.isShow = stepComponent.step === this.currentStep;
        });
    }

    private setActiveLinePosition(left: number, width: number) {
        this.renderer.setStyle(this.activeLine.nativeElement, 'left', left + 'px');
        this.renderer.setStyle(this.activeLine.nativeElement, 'width', width + 'px');
    }

    private updateActiveLinePosition() {
        const currentStepElement = this.stepHeaderContainer.nativeElement.querySelectorAll('li')[this.currentStep];
        if (currentStepElement) {
            this.renderer.setStyle(this.activeLine.nativeElement, 'left', currentStepElement.offsetLeft + 'px');
            this.renderer.setStyle(this.activeLine.nativeElement, 'width', currentStepElement.getBoundingClientRect().width + 'px');
        }
    }
}
