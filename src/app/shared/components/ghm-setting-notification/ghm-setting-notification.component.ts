import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {BaseFormComponent} from '../../../base-form.component';
import {SettingNotification} from './setting-notification.model';
import {NhModalComponent} from '../nh-modal/nh-modal.component';
import {GhmSettingNotificationService} from './ghm-setting-notification.service';
import {finalize} from 'rxjs/operators';
import {ActionResultViewModel} from '../../view-models/action-result.viewmodel';

@Component({
    selector: 'ghm-setting-notification',
    templateUrl: './ghm-setting-notification.component.html'
})
export class GhmSettingNotificationComponent extends BaseFormComponent implements OnInit {
    @ViewChild('settingNotificationModal',  {static: false}) settingNotificationModal: NhModalComponent;

    constructor(private fb: FormBuilder, private settingNotificationService: GhmSettingNotificationService) {
        super();

    }

    @Input() taskId;
    @Input() url;
    isNotification;

    @Input()
    set selectedNotification(value: any) {
        if (value != null) {
            this.settingNotificationModel = value;
            if (value.isNotification === true) {
                this.isNotification = '1';
            }

            this.buildForm();
        }
    }


    listHour: any[] = [];
    listMinute: any[] = [];
    settingNotificationModel: SettingNotification = new SettingNotification();

    ngOnInit() {
        this.isNotification = '0';
        this.renderTime();
        this.buildForm();
    }

    onModalHidden() {
        this.model.reset(new SettingNotification());
        if (this.isModified) {
            this.saveSuccessful.emit();
        }
    }

    open() {
        this.settingNotificationModal.open();
    }

    save() {
        const isValid = this.validateModel();
        if (isValid) {
            this.isSaving = true;
            this.settingNotificationService.update(this.url, this.model.value)
                .pipe(finalize(() => this.isSaving = false)).subscribe((result: ActionResultViewModel) => {
                this.settingNotificationModel.concurrencyStamp = result.data;
                this.model.value.concurrencyStamp = result.data;
                console.log(this.model.value);
                this.saveSuccessful.emit(this.model.value);
                this.settingNotificationModal.dismiss();
            });

        }
    }

    updateIsNotification() {
        this.isSaving = true;
        const settingNotification = new SettingNotification();
        settingNotification.isNotification = false;
        settingNotification.concurrencyStamp = this.settingNotificationModel.concurrencyStamp;
        this.settingNotificationService.update(this.url, settingNotification)
            .pipe(finalize(() => this.isSaving = false)).subscribe((result: ActionResultViewModel) => {
            this.settingNotificationModel.concurrencyStamp = result.data;
            settingNotification.concurrencyStamp = result.data;
            this.saveSuccessful.emit(settingNotification);
            this.settingNotificationModal.dismiss();
        });
    }

    setBuildForm() {
        if (this.isNotification) {
            this.buildForm();
        }
    }

    buildForm() {
        this.formErrors = this.renderFormError(['dates', 'hours', 'minutes']);
        this.validationMessages = this.renderFormErrorMessage([
            {'dates': ['required']},
            {'hours': ['required']},
            {'minutes': ['required']}
        ]);

        this.model = this.fb.group({
            isNotification: [true, [
                Validators.required
            ]],
            dates: [this.settingNotificationModel.dates, [
                Validators.required
            ]],
            hours: [this.settingNotificationModel.hours, [
                Validators.required
            ]],
            minutes: [this.settingNotificationModel.minutes, [
                Validators.required
            ]],
            isSendNotificationEmail: [this.settingNotificationModel.isSendNotificationEmail],
            isSendNotification: [this.settingNotificationModel.isSendNotification],
            taskId: [this.taskId],
            concurrencyStamp: [this.settingNotificationModel.concurrencyStamp],
            taskName: [this.settingNotificationModel.taskName]
        });

        this.subscribers.modelValueChanges = this.model.valueChanges.subscribe(() => this.validateModel(false));
    }

    renderTime() {
        for (let i = 0; i <= 24; i++) {
            const item = {id: i, name: i.toString() + ' Giờ'};
            this.listHour.push(item);
        }
        for (let i = 0; i < 60; i++) {
            const item = {id: i, name: i.toString() + ' Phút'};
            this.listMinute.push(item);
        }
    }
}
