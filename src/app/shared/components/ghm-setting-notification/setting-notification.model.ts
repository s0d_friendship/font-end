export class SettingNotification {
    isNotification: boolean;
    dates: Date;
    hours: number;
    minutes: number;
    isSendNotificationEmail: boolean;
    isSendNotification: boolean;
    concurrencyStamp: string;
    taskId: number;
    taskName: string;
}
