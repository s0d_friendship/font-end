import {Inject, Injectable} from '@angular/core';
import {APP_CONFIG, IAppConfig} from '../../../configs/app.config';
import {HttpClient} from '@angular/common/http';
import {SettingNotification} from './setting-notification.model';
import {Observable} from 'rxjs';
import {ActionResultViewModel} from '../../view-models/action-result.viewmodel';
import {environment} from '../../../../environments/environment';
import {SpinnerService} from '../../../core/spinner/spinner.service';
import {finalize, map} from 'rxjs/operators';
import {ToastrService} from 'ngx-toastr';

@Injectable()

export class GhmSettingNotificationService {
    constructor(private http: HttpClient, private spinnerService: SpinnerService, private toastrService: ToastrService) {
    }

    update(urlApi: string, settingNotification: SettingNotification): Observable<ActionResultViewModel> {
        const url = `${environment.apiGatewayUrl}${urlApi}`;
        this.spinnerService.show();
        return this.http.post(url, settingNotification)
            .pipe(finalize(() => this.spinnerService.hide()), map((result: ActionResultViewModel) => {
                this.toastrService.success(result.message);
                return result;
            })) as Observable<ActionResultViewModel>;
    }
}
