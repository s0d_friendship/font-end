import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CoreModule} from '../../../core/core.module';
import {NhModalModule} from '../nh-modal/nh-modal.module';
import {GhmSettingNotificationComponent} from './ghm-setting-notification.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {GhmSelectModule} from '../ghm-select/ghm-select.module';
import {GhmSettingNotificationService} from './ghm-setting-notification.service';
import {NhDateModule} from '../nh-datetime-picker/nh-date.module';
import {MatCheckboxModule, MatRadioModule} from '@angular/material';

@NgModule({
    imports: [
        CommonModule, CoreModule, NhModalModule, MatRadioModule, MatCheckboxModule,
        FormsModule, GhmSelectModule, NhDateModule, ReactiveFormsModule
    ],
    declarations: [GhmSettingNotificationComponent],
    exports: [GhmSettingNotificationComponent],
    providers: [GhmSettingNotificationService]
})
export class GhmSettingNotificationModule {
}
