import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GhmSettingNotificationComponent } from './ghm-setting-notification.component';

describe('GhmSettingNotificationComponent', () => {
  let component: GhmSettingNotificationComponent;
  let fixture: ComponentFixture<GhmSettingNotificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GhmSettingNotificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GhmSettingNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
