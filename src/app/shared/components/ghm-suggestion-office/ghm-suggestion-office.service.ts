import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { APP_CONFIG, IAppConfig } from '../../../configs/app.config';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SearchResultViewModel } from '../../view-models/search-result.viewmodel';
import { environment } from '../../../../environments/environment';
import { NhSuggestion } from '../nh-suggestion/nh-suggestion.component';

@Injectable()
export class GhmSuggestionOfficeService {
    constructor(@Inject(APP_CONFIG) private appConfig: IAppConfig,
                private http: HttpClient) {
    }

    search(keyword: string, page = 1, pageSize = 20): Observable<SearchResultViewModel<NhSuggestion>> {
        const url = `${environment.apiGatewayUrl}api/v1/hr/offices/suggestions`;
        return this.http
            .get(url, {
                params: new HttpParams()
                    .set('keyword', keyword ? keyword : '')
                    .set('page', page ? page.toString() : '')
                    .set('pageSize', pageSize ? pageSize.toString() : '')
            }) as Observable<SearchResultViewModel<NhSuggestion>>;
    }

    stripToVietnameChar(str): string {
        str = str.toLowerCase();
        str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
        str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
        str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
        str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
        str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
        str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
        str = str.replace(/đ/g, 'd');
        return str;
    }
}
