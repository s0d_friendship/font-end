import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {GhmSuggestionOfficeComponent} from './ghm-suggestion-office.component';
import {NhSuggestionModule} from '../nh-suggestion/nh-suggestion.module';
import {GhmSuggestionOfficeService} from './ghm-suggestion-office.service';

@NgModule({
    declarations: [GhmSuggestionOfficeComponent],
    imports: [
        CommonModule, NhSuggestionModule
    ],
    exports: [GhmSuggestionOfficeComponent],
    providers: [GhmSuggestionOfficeService]
})
export class GhmSuggestionOfficeModule {
}
