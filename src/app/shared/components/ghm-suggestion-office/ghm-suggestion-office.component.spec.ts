import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GhmSuggestionOfficeComponent } from './ghm-suggestion-office.component';

describe('GhmSuggestionOfficeComponent', () => {
  let component: GhmSuggestionOfficeComponent;
  let fixture: ComponentFixture<GhmSuggestionOfficeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GhmSuggestionOfficeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GhmSuggestionOfficeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
