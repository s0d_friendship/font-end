import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {NhSuggestion, NhSuggestionComponent} from '../nh-suggestion/nh-suggestion.component';
import {GhmSuggestionOfficeService} from './ghm-suggestion-office.service';
import {SearchResultViewModel} from '../../view-models/search-result.viewmodel';
import {finalize} from 'rxjs/operators';
import {BaseListComponent} from '../../../base-list.component';

@Component({
    selector: 'app-ghm-suggestion-office',
    templateUrl: './ghm-suggestion-office.component.html',
    styleUrls: ['./ghm-suggestion-office.component.css']
})
export class GhmSuggestionOfficeComponent extends BaseListComponent<NhSuggestion> implements OnInit {
    @ViewChild(NhSuggestionComponent,  {static: true}) nhSuggestionComponent: NhSuggestionComponent;
    @Input() multiple = false;
    @Input() selectedItem;
    @Input() selectedUser;
    @Input() readonly = false;
    @Output() keyPressed = new EventEmitter();
    @Output() itemSelected = new EventEmitter();
    @Output() itemRemoved = new EventEmitter();

    constructor(private officeSuggestionService: GhmSuggestionOfficeService) {
        super();
    }

    ngOnInit() {
    }

    onItemSelected(item: any) {
        this.itemSelected.emit(item);
    }

    onSearchKeyPress(keyword: string) {
        this.keyPressed.emit(keyword);
        this.keyword = keyword;
        this.search(1);
    }

    onNextPageClick(data: any) {
        this.keyword = data.keyword;
        this.pageSize = data.pageSize;
        this.search(data.page, true);
    }

    search(currentPage: number, isAppend = false) {
        this.isSearching = true;
        this.currentPage = currentPage;
        this.officeSuggestionService.search(this.keyword, this.currentPage, this.appConfig.PAGE_SIZE)
            .pipe(finalize(() => this.isSearching = false))
            .subscribe((result: SearchResultViewModel<NhSuggestion>) => {
                this.totalRows = result.totalRows;
                if (!isAppend) {
                    this.listItems = result.items;
                } else {
                    this.listItems.push(...result.items);
                    this.nhSuggestionComponent.updateScrollPosition();
                }
            });
    }

    clear() {
        this.nhSuggestionComponent.clear();
    }
}
