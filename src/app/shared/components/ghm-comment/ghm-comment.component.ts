import {AfterViewInit, Component, ElementRef, EventEmitter, Input, OnInit, Output, Renderer, ViewChild} from '@angular/core';
import * as _ from 'lodash';
import * as moment from 'moment';
import {BaseListComponent} from '../../../base-list.component';
import {ToastrService} from 'ngx-toastr';
import {GhmCommentService} from './ghm-comment.service';
import {CommentModel} from './comment.model';
import {AuthService} from '../../services/auth.service';
import {UtilService} from '../../services/util.service';
import {SearchResultViewModel} from '../../view-models/search-result.viewmodel';
import {ActionResultViewModel} from '../../view-models/action-result.viewmodel';

@Component({
    selector: 'ghm-comment',
    templateUrl: './ghm-comment.component.html',
    styleUrls: ['./ghm-comment.component.scss']
})
export class GhmCommentComponent extends BaseListComponent<CommentModel> implements OnInit, AfterViewInit {
    @ViewChild('commentsContainer',  {static: true}) commentsContainer: ElementRef;
    @Input() url: string;
    @Input() objectId: number;
    @Input() objectType: number;
    @Input() showCommentBox = true;
    @Input() isEnable = true;
    @Output() onSendCommentComplete = new EventEmitter();
    @Output() onRemoveComment = new EventEmitter();
    parentId: number;
    errorMessage: string;
    isShowLoadMore = false;
    totalPage = 0;
    isSaving = false;
    boxId = `comment-box-${new Date().getTime()}`;
    comment: string;
    currentUser;
    listComments = [];
    commentViewModel: CommentModel;

    constructor(private renderer: Renderer,
                private commentService: GhmCommentService,
                private utilService: UtilService,
                private toastr: ToastrService, private authService: AuthService) {
        super();
        this.currentUser = this.appService.currentUser;
    }

    ngOnInit() {
    }

    ngAfterViewInit() {
    }

    searchByObjectId(objectType: number, objectId: number) {
        this.objectType = objectType;
        this.objectId = objectId;
        setTimeout(() => {
            this.search(false);
        });

    }

    search(isSetScroll, isNew = false) {
        if (isNew) {
            this.currentPage = 1;
        }
        this.isSearching = true;
        this.errorMessage = '';
        this.commentService.searchByObjectId(this.objectType, this.objectId, this.currentPage, this.pageSize)
            .subscribe((result: SearchResultViewModel<CommentModel>) => {
                this.renderListComment(result.items, isSetScroll, isNew);
                this.totalRows = result.totalRows;
                this.totalPage = Math.ceil(this.totalRows / this.pageSize);
                this.isSearching = false;
                this.checkShowHideLoadMore();
            });
    }

    showCommentInput() {
        this.showCommentBox = !this.showCommentBox;
        this.utilService.focusElement('commentInput');
    }

    loadMore() {
        this.currentPage = this.currentPage + 1;
        this.search(false);
    }

    loadMoreChildren() {
    }

    sendComment() {
        this.errorMessage = '';
        if (!this.comment || this.comment.trim() === '') {
            this.toastr.error('Vui lòng nhập nội dung bình luận');
            return;
        }

        this.commentViewModel = new CommentModel();
        this.commentViewModel.content = this.comment;
        this.commentViewModel.objectType = this.objectType;
        this.commentViewModel.objectId = this.objectId;
        this.isSaving = true;
        this.commentService.insert(this.commentViewModel)
            .subscribe((result: ActionResultViewModel<CommentModel>) => {
                this.isSaving = false;
                const comment = {
                    id: result.data.id,
                    userId: result.data.userId,
                    fullName: this.currentUser.fullName,
                    image: this.currentUser.avatar,
                    createTime: moment().fromNow(),
                    content: this.comment
                };
                this.listComments.push(comment);
                this.comment = '';
                this.setScroll();
                this.onSendCommentComplete.emit();
            });
    }

    focusSendComment() {
        this.utilService.focusElement('commentInput');
    }

    removeComment(commnet: CommentModel) {
        this.commentService.delete(commnet.id).subscribe((result: ActionResultViewModel) => {
            _.remove(this.listComments, (item: CommentModel) => {
                return item.id === commnet.id;
            });
            this.onRemoveComment.emit();
        });
    }

    private renderListComment(comments, isSetScroll: boolean, isNew: boolean) {
        if (comments && comments.length > 0) {
            if (isNew) {
                this.listComments = [];
            }

            _.each(comments, (comment: any) => {
                comment.createTime = moment(comment.createTime,
                    this.appService.momentDateTimeLocalFormat[this.appService.locale].s).fromNow();
                this.listComments.unshift(comment);
            });

            if (isSetScroll) {
                this.setScroll();
            }
        } else {
            this.totalPage = 0;
            this.totalRows = 0;
            this.currentPage = 1;
            this.listComments = [];
        }
    }

    private checkShowHideLoadMore() {
        this.isShowLoadMore = this.currentPage < this.totalPage && this.listComments.length > 0;
    }

    private setScroll() {
        setTimeout(() => {
            const commentsContainerHeight = (<HTMLElement>this.commentsContainer.nativeElement).scrollHeight;
            this.commentsContainer.nativeElement.scrollTop = commentsContainerHeight + 50;
        }, 200);
    }
}
