import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {GhmCommentComponent} from './ghm-comment.component';
import {NhImageModule} from '../nh-image/nh-image.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {GhmCommentService} from './ghm-comment.service';
import {AuthService} from '../../services/auth.service';
import {DatetimeFormatModule} from '../../pipe/datetime-format/datetime-format.module';
import {CoreModule} from '../../../core/core.module';

@NgModule({
    imports: [
        CommonModule, NhImageModule, CoreModule,
        FormsModule, ReactiveFormsModule, DatetimeFormatModule,
    ],
    declarations: [GhmCommentComponent],
    providers: [GhmCommentService, AuthService],
    exports: [GhmCommentComponent]
})
export class GhmCommentModule {
}
