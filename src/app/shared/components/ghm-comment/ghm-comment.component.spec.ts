import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GhmCommentComponent } from './ghm-comment.component';

describe('GhmCommentComponent', () => {
  let component: GhmCommentComponent;
  let fixture: ComponentFixture<GhmCommentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GhmCommentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GhmCommentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
