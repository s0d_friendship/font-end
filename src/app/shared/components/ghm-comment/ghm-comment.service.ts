import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {APP_CONFIG, IAppConfig} from '../../../configs/app.config';
import {HttpClient, HttpParams} from '@angular/common/http';
import {SpinnerService} from '../../../core/spinner/spinner.service';
import {ToastrService} from 'ngx-toastr';
import {SearchResultViewModel} from '../../view-models/search-result.viewmodel';
import {ActionResultViewModel} from '../../view-models/action-result.viewmodel';
import {CommentModel} from './comment.model';
import {map} from 'rxjs/operators';
import {environment} from '../../../../environments/environment';

@Injectable()
export class GhmCommentService {
    url = `${environment.apiGatewayUrl}api/v1/task/comments`;

    constructor(@Inject(APP_CONFIG) private appConfig: IAppConfig, private http: HttpClient,
                private spinnerService: SpinnerService, private toastrService: ToastrService) {
    }

    searchByObjectId(objectType: number, objectId: number, page: number = 1,
                     pageSize: number = this.appConfig.PAGE_SIZE): Observable<SearchResultViewModel<CommentModel>> {
        const params = new HttpParams()
            .set('page', page ? page.toString() : '1')
            .set('pageSize', pageSize ? pageSize.toString() : this.appConfig.PAGE_SIZE.toString());

        return this.http.get(`${this.url}/${objectType}/${objectId}`, {
            params: params
        }) as Observable<SearchResultViewModel<CommentModel>>;
    }

    insert(comment: CommentModel): Observable<ActionResultViewModel<CommentModel>> {
        return this.http.post(this.url, comment).pipe() as Observable<ActionResultViewModel<CommentModel>>;
    }

    delete(id: number): Observable<ActionResultViewModel> {
        return this.http.delete(`${this.url}/${id}`).pipe(map((result: ActionResultViewModel) => {
            this.toastrService.success(result.message);
            return result;
        }))  as Observable<ActionResultViewModel>;
    }
}
