export class CommentModel {
    id: number;
    userId: string;
    fullName: string;
    content: string;
    objectType: number;
    objectId: number;
    parentId: number;
    createTime: string;
    childCount: number;
    likeCount: number;
    image: string;
    urlAttachment: string;
}
