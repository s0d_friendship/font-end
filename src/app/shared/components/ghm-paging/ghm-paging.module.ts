import { NgModule } from '@angular/core';
import { GhmPagingComponent } from './ghm-paging.component';
import { CommonModule } from '@angular/common';
import {GhmSelectModule} from '../ghm-select/ghm-select.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
    imports: [CommonModule, GhmSelectModule, FormsModule],
    exports: [GhmPagingComponent],
    declarations: [GhmPagingComponent],
    providers: [],
})
export class GhmPagingModule {
}
