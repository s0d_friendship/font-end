import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {GhmColorPickerComponent} from './ghm-color-picker.component';

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [GhmColorPickerComponent],
    exports: [GhmColorPickerComponent]
})
export class GhmColorPickerModule {
}
