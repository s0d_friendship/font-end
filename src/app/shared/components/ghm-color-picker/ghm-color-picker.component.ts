import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import * as _ from 'lodash';

@Component({
    selector: 'ghm-color-picker',
    templateUrl: './ghm-color-picker.component.html',
    styleUrls: ['./ghm-color-picker.component.scss']
})

export class GhmColorPickerComponent implements OnInit {
    @Input() colorId;
    @Output() selectColor = new EventEmitter();

    listColorLabel = [
        {id: '#fad390'},
        {id: '#f8c291'},
        {id: '#6a89cc'},
        {id: '#82ccdd'},
        {id: '#b8e994'},
        {id: '#f6b93b'},
        {id: '#e55039'},
        {id: '#4a69bd'},
        {id: '#60a3bc'},
        {id: '#78e08f'},
        {id: '#fa983a'},
        {id: '#eb2f06'},
        {id: '#1e3799'},
        {id: '#3c6382'},
        {id: '#3498db'},
        {id: '#e58e26'},
        {id: '#b71540'},
        {id: '#0c2461'},
        {id: '#0a3d62'},
        {id: '#079992'}
    ];

    constructor() {
    }

    ngOnInit() {
    }

    selectedItem(item: any) {
        _.each(this.listColorLabel, (color: any) => {
            color.selected = false;
            if (color.id === item.id) {
                this.colorId = color.id;
                this.selectColor.emit(this.colorId);
            }
        });
    }
}
