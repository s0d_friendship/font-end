import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GhmColorPickerComponent } from './ghm-color-picker.component';

describe('GhmColorPickerComponent', () => {
  let component: GhmColorPickerComponent;
  let fixture: ComponentFixture<GhmColorPickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GhmColorPickerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GhmColorPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
