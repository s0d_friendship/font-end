import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {GhmDialogComponent} from './ghm-dialog.component';
import {GhmDialogHeaderComponent} from './ghm-dialog-header.component';
import {GhmDialogContentComponent} from './ghm-dialog-content.component';
import {GhmDialogFooterComponent} from './ghm-dialog-footer.component';
import {GhmDialogService} from './ghm-dialog-service';
import {GhmDismissDirective} from './ghm-dismiss.directive';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {GhmDialogTriggerDirectiveDirective} from './ghm-dialog-trigger-directive.directive';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule
    ],
    declarations: [GhmDialogComponent, GhmDialogHeaderComponent, GhmDialogContentComponent, GhmDialogFooterComponent, GhmDismissDirective, GhmDialogTriggerDirectiveDirective],
    exports: [GhmDialogComponent, GhmDialogFooterComponent, GhmDialogContentComponent, GhmDialogHeaderComponent, GhmDismissDirective, GhmDialogTriggerDirectiveDirective],
    providers: [GhmDialogService]
})
export class GhmDialogModule {
}
