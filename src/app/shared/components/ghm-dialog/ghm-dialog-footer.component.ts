import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ghm-dialog-footer',
  template: `<ng-content></ng-content>`,
})
export class GhmDialogFooterComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
