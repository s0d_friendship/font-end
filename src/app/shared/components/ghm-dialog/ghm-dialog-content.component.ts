import {Component, Input, OnInit} from '@angular/core';

@Component({
    selector: 'ghm-dialog-content',
    template: `
        <div [ngStyle]="{'overflow' : isOverflow ? 'auto' : '', 'height': height ? height + 'px' : 'inset'}" style=" max-height: 300px;">
            <ng-content></ng-content>
        </div>`,
})
export class GhmDialogContentComponent implements OnInit {
    @Input() isOverflow = false;
    @Input() height;

    constructor() {
    }

    ngOnInit() {
    }

}
