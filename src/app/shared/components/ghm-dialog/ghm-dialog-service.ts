import {ElementRef, Injectable} from '@angular/core';
import {Subject} from 'rxjs';
import {GhmDialogTriggerDirectiveDirective} from './ghm-dialog-trigger-directive.directive';
import * as _ from 'lodash';

@Injectable()
export class GhmDialogService {
    dismiss$ = new Subject();
    private ghmDialogTriggerDirectives: GhmDialogTriggerDirectiveDirective[] = [];
    private listDialogs: ElementRef[] = [];

    constructor() {
    }

    add(ghmDialogComponent: ElementRef) {
        this.listDialogs.push(ghmDialogComponent);
    }

    dismiss(e: MouseEvent) {
        this.dismiss$.next(e);
    }

    remove(el: ElementRef) {
        const index = this.listDialogs.indexOf(el);
        this.listDialogs.splice(index, 1);
    }
}
