import {
    AfterViewInit,
    Component,
    ContentChild,
    ContentChildren,
    ElementRef,
    EventEmitter,
    HostListener,
    Input,
    OnInit,
    Optional,
    Output,
    QueryList,
    Renderer2,
    SkipSelf,
    TemplateRef,
    ViewChild,
    ViewContainerRef,
    ViewEncapsulation
} from '@angular/core';
import {GhmDialogHeaderComponent} from './ghm-dialog-header.component';
import {GhmDialogFooterComponent} from './ghm-dialog-footer.component';
import {GhmDialogService} from './ghm-dialog-service';
import {GlobalPositionStrategy, Overlay, OverlayRef} from '@angular/cdk/overlay';
import {TemplatePortal} from '@angular/cdk/portal';

@Component({
    selector: 'ghm-dialog',
    templateUrl: './ghm-dialog.component.html',
    styleUrls: ['./ghm-dialog.component.scss'],
    encapsulation: ViewEncapsulation.None,
})

export class GhmDialogComponent implements OnInit, AfterViewInit {
    @ContentChild(GhmDialogHeaderComponent , {static: true}) dialogHeaderComponent: GhmDialogHeaderComponent;
    @ContentChild(GhmDialogFooterComponent , {static: true}) dialogFooterComponents: GhmDialogFooterComponent;
    @ContentChildren(GhmDialogComponent) modelComponents: QueryList<GhmDialogComponent>;
    @ViewChild('contentDialogRef' , {static: true}) contentRef: TemplateRef<any>;
    @Input() backdropStatic = false;
    @Input() title = '';
    @Input() size = 'sm';
    @Input() position = ''; // center
    @Input() elementId = '';
    @Output() show = new EventEmitter();
    @Output() hide = new EventEmitter();

    isActive;

    private overlayRef: OverlayRef;
    private positionStrategy = new GlobalPositionStrategy();

    ghmDialogData: any;

    constructor(private el: ElementRef,
                private renderer: Renderer2,
                private overlay: Overlay,
                private viewContainerRef: ViewContainerRef,
                @SkipSelf() @Optional() private ghmDialogService: GhmDialogService) {
    }

    ngOnInit() {
        // this.overlayRef = this.overlay.create({
        //     positionStrategy: this.positionStrategy
        // });
        // this.overlayRef.backdropClick().subscribe(event => {
        //     if (!this.backdropStatic) {
        //         this.dismiss();
        //     }
        // });
    }

    ngAfterViewInit() {
        this.ghmDialogService.dismiss$.subscribe((event: any) => {
            if (this.overlayRef && this.overlayRef.overlayElement) {
                const dialogElement = this.overlayRef.overlayElement.getElementsByClassName('ghm-dialog')[0];
                if (dialogElement && dialogElement.contains(event.target)) {
                    this.dismiss();
                    this.hide.emit();
                }
            }
            // this.dismiss();
        });
    }

    @HostListener('window:keyup', ['$event'])
    onWindowKeyup(event: KeyboardEvent) {
        // Escape press: Close modal.
        if (event.key === 'Escape') {
            this.dismiss();
        }
    }

    @HostListener('document:click', ['$event'])
    onClick(event) {
        if (this.overlayRef && this.overlayRef.overlayElement) {
            const dialogElement = this.overlayRef.overlayElement.getElementsByClassName('ghm-dialog')[0];
            if (dialogElement && !dialogElement.contains(event.target)
                && !this.el.nativeElement.contains(event.target) && !this.backdropStatic) {
                this.dismiss();
            }
        }
    }

    active(el: ElementRef) {
        this.overlayRef = this.overlay.create({
            positionStrategy: this.positionStrategy
        });
        this.overlayRef.backdropClick().subscribe(event => {
            if (!this.backdropStatic) {
                this.dismiss();
            }
        });

        setTimeout(() => {
            if (this.overlayRef) {
                if (!this.overlayRef.hasAttached()) {
                    this.overlayRef.attach(new TemplatePortal(this.contentRef, this.viewContainerRef));
                    const clientRect = el.nativeElement.getBoundingClientRect();
                    const dialog = this.overlayRef.overlayElement.getElementsByClassName('ghm-dialog')[0];
                    const dialogHeight = this.overlayRef.overlayElement.getElementsByClassName('ghm-dialog')[0].clientHeight;
                    const windowWidth = window.innerWidth;
                    const windowHeight = window.innerHeight;
                    const isLeft = windowWidth - (clientRect.left + this.getSizeDialog(this.size)) > 0;
                    const isTop = windowHeight - (clientRect.top + clientRect.height + dialogHeight + 50) < 0;
                    const left = isLeft ? clientRect.left : clientRect.left - this.getSizeDialog(this.size) / 2 - 30;
                    const top = isTop ? clientRect.top - dialogHeight - 10 : clientRect.top + clientRect.height;
                    this.positionStrategy.left(`${left}px`);
                    this.positionStrategy.top(`${top}px`);
                    this.renderer.addClass(dialog, isTop ? 'ghm-dialog-top' : 'ghm-dialog-bottom');
                    this.renderer.addClass(dialog, isLeft ? 'ghm-dialog-left' : 'ghm-dialog-right');

                    if (this.position === 'center') {
                        this.renderer.addClass(dialog, `ghm-dialog-${this.size}-center`);
                        this.positionStrategy.left(`${clientRect.right - this.getSizeDialog(this.size) / 2 - clientRect.width / 2 - 8}px`);
                    }
                    this.positionStrategy.apply();
                }
            }

            this.ghmDialogService.add(el);
            this.show.emit(this.ghmDialogData);
        });
    }

    public dismiss() {
        if (this.overlayRef && this.overlayRef.hasAttached()) {
            this.overlayRef.detach();
            this.ghmDialogService.remove(this.el);
            this.hide.emit();
        }
    }

    private getSizeDialog(size) {
        switch (size) {
            case 'sm':
                return 300;
            case 'md':
                return 600;
            case 'lg':
                return 900;
        }
    }
}
