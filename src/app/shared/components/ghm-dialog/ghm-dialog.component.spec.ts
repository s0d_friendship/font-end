import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GhmDialogComponent } from './ghm-dialog.component';

describe('GhmDialogComponent', () => {
  let component: GhmDialogComponent;
  let fixture: ComponentFixture<GhmDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GhmDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GhmDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
