import {Component, Input, OnInit} from '@angular/core';
import {GhmDialogService} from './ghm-dialog-service';

@Component({
    selector: 'ghm-dialog-header',
    template: `
        <div class="ghm-dialog-header-content">
            <ng-content></ng-content>
        </div>
        <div class="ghm-dialog-header-close-button" imgsrc="dlg-close" ghm-dismiss="">
        </div>`,
})
export class GhmDialogHeaderComponent implements OnInit {
    @Input() showCloseButton = true;

    constructor(private ghmDialogService: GhmDialogService) {
    }

    ngOnInit() {
    }
}
