import {
    Directive,
    ElementRef,
    Input,
    OnDestroy,
    OnInit,
    Optional,
    Renderer2,
    SkipSelf,
    TemplateRef,
    ViewChild,
    ViewContainerRef
} from '@angular/core';
import {GhmDialogComponent} from './ghm-dialog.component';
import {GlobalPositionStrategy, Overlay, OverlayRef} from '@angular/cdk/overlay';
import {GhmDialogService} from './ghm-dialog-service';

@Directive({
    selector: '[ghmDialogTrigger]'
})

export class GhmDialogTriggerDirectiveDirective implements OnInit, OnDestroy {
    @ViewChild('contentDialogRef',  {static: true}) contentRef: TemplateRef<any>;
    @Input() ghmDialogData: any;
    @Input() ghmDialogTriggerFor: GhmDialogComponent;
    @Input() ghmDialogEvent = '';

    private positionStrategy = new GlobalPositionStrategy();
    private overlayRef: OverlayRef;

    constructor(private el: ElementRef,
                private renderer: Renderer2,
                private overlay: Overlay,
                private viewContainerRef: ViewContainerRef,
                @SkipSelf() @Optional() private ghmDialogService: GhmDialogService) {
    }

    ngOnInit() {
        this.renderer.listen(this.el.nativeElement, this.ghmDialogEvent ? this.ghmDialogEvent : 'click', (event: MouseEvent) => {
            event.preventDefault();
            event.stopPropagation();
            this.ghmDialogTriggerFor.active(this.el);
            if (this.ghmDialogData) {
                this.ghmDialogTriggerFor.ghmDialogData = this.ghmDialogData;
            }
        });

        // this.overlayRef = this.overlay.create({
        //     positionStrategy: this.positionStrategy
        // });
        // this.ghmDialogService.add(this.el);
    }

    ngOnDestroy() {
        this.ghmDialogTriggerFor.dismiss();
    }
}
