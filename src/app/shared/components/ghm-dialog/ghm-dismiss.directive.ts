import {Directive, ElementRef, EventEmitter, HostListener, Output} from '@angular/core';
import {GhmDialogService} from './ghm-dialog-service';

@Directive({
    selector: '[ghm-dismiss]'
})

export class GhmDismissDirective {
    @Output() dismiss = new EventEmitter();

    constructor(private ghmDialogService: GhmDialogService) {
    }

    @HostListener('click', ['$event'])
    onElementClick(e: MouseEvent) {
        this.ghmDialogService.dismiss(e);
    }

}
