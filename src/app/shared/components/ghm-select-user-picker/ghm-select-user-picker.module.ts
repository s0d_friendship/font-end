import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CoreModule} from '../../../core/core.module';
import {GhmSelectModule} from '../ghm-select/ghm-select.module';
import {NhUserPickerModule} from '../nh-user-picker/nh-user-picker.module';
import {GhmSelectUserPickerComponent} from './ghm-select-user-picker.component';
import {NhModalModule} from '../nh-modal/nh-modal.module';
import {GhmSuggestionUserModule} from '../ghm-suggestion-user/ghm-suggestion-user.module';

@NgModule({
    imports: [
        CommonModule, CoreModule, GhmSelectModule, NhUserPickerModule, NhModalModule, GhmSuggestionUserModule
    ],
    declarations: [GhmSelectUserPickerComponent],
    exports: [GhmSelectUserPickerComponent]
})
export class GhmSelectUserPickerModule {
}
