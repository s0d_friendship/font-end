import {Component, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {NhUserPickerComponent} from '../nh-user-picker/nh-user-picker.component';
import {NhUserPicker} from '../nh-user-picker/nh-user-picker.model';
import * as _ from 'lodash';
import {NhModalComponent} from '../nh-modal/nh-modal.component';
import {GhmSuggestionUserComponent} from '../ghm-suggestion-user/ghm-suggestion-user.component';

@Component({
    selector: 'ghm-select-user-picker',
    templateUrl: './ghm-select-user-picker.component.html',
    styleUrls: ['./ghm-select-user-picker.scss']
})
export class GhmSelectUserPickerComponent {
    @ViewChild(NhUserPickerComponent,  {static: true}) userPickerComponent: NhUserPickerComponent;
    @ViewChild('ghmSelectUserSuggestion',  {static: false}) ghmSelectUserSuggestion: GhmSuggestionUserComponent;
    @ViewChild('userPickerModal',  {static: false}) userPickerModal: NhModalComponent;
    @Input() title = 'Select participant';
    @Input() selectUsers = [];
    @Output() itemSelectUser = new EventEmitter();
    @Output() acceptSelectUsers = new EventEmitter();

    urlUser;
    listUserPickerInsert;
    listUserPickerRemove;

    constructor() {
        this.urlUser = `${environment.apiGatewayUrl}api/v1/hr/users/suggestions`;
    }

    selectUser(value: any) {
        if (value) {
            this.ghmSelectUserSuggestion.clear();
            this.itemSelectUser.emit(value.data);
        }
    }

    showUserPicker() {
        this.userPickerModal.open();
        setTimeout(() => {
            this.userPickerComponent.selectedItems = this.selectUsers;
            this.listUserPickerInsert = [];
            this.listUserPickerRemove = [];
            this.userPickerComponent.show();
        });
    }

    selectUserPicker(userPicker: NhUserPicker) {
        const userSelectInfo = _.find(this.selectUsers, (user: NhUserPicker) => {
            return user.id === userPicker.id;
        });

        if (!userSelectInfo) {
            const userPickerInsertInfo = _.find(this.listUserPickerInsert, (userInsert: NhUserPicker) => {
                return userInsert.id === userPicker.id;
            });

            if (!userPickerInsertInfo) {
                this.listUserPickerInsert.push(userPicker);
            }

            const userPickerRemoveInfo = _.find(this.listUserPickerRemove, (userRemove: NhUserPicker) => {
                return userRemove.id === userPicker.id;
            });

            if (userPickerRemoveInfo) {
                _.remove(this.listUserPickerRemove, (item: NhUserPicker) => {
                    return item.id === userPicker.id;
                });
            }
        }
    }

    // Xóa user picker
    removeUserPicker(userPicker: NhUserPicker) {
        const userPickerRemoveInfo = _.find(this.listUserPickerRemove, (userRemove: NhUserPicker) => {
            return userRemove.id === userPicker.id;
        });

        if (!userPickerRemoveInfo) {
            this.listUserPickerRemove.push(userPicker);
        }

        const userPickerInsertInfo = _.find(this.listUserPickerInsert, (userInsert: NhUserPicker) => {
            return userInsert.id === userPicker.id;
        });

        if (userPickerInsertInfo) {
            _.remove(this.listUserPickerInsert, (item: NhUserPicker) => {
                return item.id === userPicker.id;
            });
        }
    }

    onAcceptSelectUsers(value: NhUserPicker[]) {
        const valueEmit = {
            listUserInsert: this.listUserPickerInsert,
            listUserRemove: this.listUserPickerRemove,
            listUser: value
        };
        this.acceptSelectUsers.emit(valueEmit);
        this.userPickerModal.dismiss();
    }

    closeUserPicker() {
        this.userPickerModal.dismiss();
    }
}
