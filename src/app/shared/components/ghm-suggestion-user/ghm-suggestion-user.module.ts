import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {CoreModule} from '../../../core/core.module';
import { NhSuggestionModule } from '../nh-suggestion/nh-suggestion.module';
import {GhmSuggestionUserComponent} from './ghm-suggestion-user.component';
import {GhmSuggestionUserService} from './ghm-suggestion-user.service';

@NgModule({
    imports: [CommonModule, FormsModule, CoreModule, NhSuggestionModule],
    exports: [GhmSuggestionUserComponent],
    declarations: [GhmSuggestionUserComponent],
    providers: [GhmSuggestionUserService],
})
export class GhmSuggestionUserModule {
}
