import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild, ViewEncapsulation} from '@angular/core';
import {finalize} from 'rxjs/operators';
import {NhSuggestion, NhSuggestionComponent} from '../nh-suggestion/nh-suggestion.component';
import {SearchResultViewModel} from '../../view-models/search-result.viewmodel';
import {BaseListComponent} from '../../../base-list.component';
import {GhmSuggestionUserService} from './ghm-suggestion-user.service';

export class UserSuggestion {
    id: string;
    fullName: string;
    officeName: string;
    positionName: string;
    avatar: string;
    isSelected: boolean;
    isActive: boolean;

    constructor(id?: string, fullName?: string, officeName?: string, positionName?: string, avatar?: string, isSelected?: boolean) {
        this.id = id;
        this.fullName = fullName;
        this.officeName = officeName;
        this.positionName = positionName;
        this.avatar = avatar;
        this.isSelected = isSelected;
        this.isActive = false;
    }
}

@Component({
    selector: 'ghm-suggestion-user',
    templateUrl: './ghm-suggestion-user.component.html',
    styleUrls: ['./ghm-suggestion-user.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class GhmSuggestionUserComponent extends BaseListComponent<NhSuggestion> implements OnInit, OnDestroy {
    @ViewChild(NhSuggestionComponent,  {static: true}) nhSuggestionComponent: NhSuggestionComponent;
    @Input() multiple = false;
    @Input() selectedItem;
    @Input() selectedUser;
    @Input() readonly = false;
    @Input() officeId;
    @Output() keyPressed = new EventEmitter();
    @Output() itemSelected = new EventEmitter();
    @Output() itemRemoved = new EventEmitter();

    constructor(private userSuggestionService: GhmSuggestionUserService) {
        super();
    }

    ngOnInit() {
    }

    onItemSelected(item: any) {
        this.itemSelected.emit(item);
    }

    onSearchKeyPress(keyword: string) {
        this.keyPressed.emit(keyword);
        this.keyword = keyword;
        this.search(1);
    }

    onNextPageClick(data: any) {
        this.keyword = data.keyword;
        this.pageSize = data.pageSize;
        this.search(data.page, true);
    }

    search(currentPage: number, isAppend = false) {
        this.isSearching = true;
        this.currentPage = currentPage;
        this.userSuggestionService.search(this.keyword, this.officeId, this.currentPage, this.appConfig.PAGE_SIZE)
            .pipe(finalize(() => this.isSearching = false))
            .subscribe((result: SearchResultViewModel<NhSuggestion>) => {
                this.totalRows = result.totalRows;
                if (!isAppend) {
                    this.listItems = result.items;
                } else {
                    this.listItems.push(...result.items);
                    this.nhSuggestionComponent.updateScrollPosition();
                }
            });
    }

    clear() {
        this.nhSuggestionComponent.clear();
    }
}
