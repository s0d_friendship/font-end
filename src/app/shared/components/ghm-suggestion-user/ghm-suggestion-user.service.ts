import {Inject, Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {APP_CONFIG, IAppConfig} from '../../../configs/app.config';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {SearchResultViewModel} from '../../view-models/search-result.viewmodel';
import {environment} from '../../../../environments/environment';
import {NhSuggestion} from '../nh-suggestion/nh-suggestion.component';
import {UserSuggestion} from './ghm-suggestion-user.component';

@Injectable()
export class GhmSuggestionUserService {
    constructor(@Inject(APP_CONFIG) private appConfig: IAppConfig,
                private http: HttpClient) {
    }

    search(keyword?: string, officeId?: number, page?: number,
           pageSize?: number): Observable<SearchResultViewModel<NhSuggestion>> {
        const url = `${environment.apiGatewayUrl}api/v1/hr/users/suggestions`;
        return this.http
            .get(url, {
                params: new HttpParams()
                    .set('keyword', keyword ? keyword : '')
                    .set('officeId', officeId ? officeId.toString() : '')
                    .set('page', page ? page.toString() : '')
                    .set('pageSize', pageSize ? pageSize.toString() : '')
            })
            .pipe(map((result: SearchResultViewModel<UserSuggestion>) => {
                return {
                    totalRows: result.totalRows,
                    items: result.items.map((item: UserSuggestion) => {
                        return {
                            id: item.id,
                            name: item.fullName,
                            description: `${item.officeName} - ${item.positionName}`,
                            isSelected: false,
                            data: item,
                            image: item.avatar
                        } as NhSuggestion;
                    })
                };
            }))as Observable<SearchResultViewModel<NhSuggestion>>;
    }

    stripToVietnameChar(str): string {
        str = str.toLowerCase();
        str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
        str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
        str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
        str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
        str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
        str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
        str = str.replace(/đ/g, 'd');
        return str;
    }
}
