import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {GhmListUserSuggestionComponent} from './ghm-list-user-suggestion.component';
import {GhmListUserSuggestionService} from './ghm-list-user-suggestion.service';
import {CoreModule} from '../../../core/core.module';

@NgModule({
    imports: [
        CommonModule, CoreModule
    ],
    declarations: [GhmListUserSuggestionComponent],
    exports: [GhmListUserSuggestionComponent],
    providers: [GhmListUserSuggestionService]
})
export class GhmListUserSuggestionModule {
}
