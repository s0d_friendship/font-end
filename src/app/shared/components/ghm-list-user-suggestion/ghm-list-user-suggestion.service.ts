import {Inject, Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {APP_CONFIG, IAppConfig} from '../../../configs/app.config';
import {Observable} from 'rxjs';
import {SearchResultViewModel} from '../../view-models/search-result.viewmodel';
import {environment} from '../../../../environments/environment';
import {UserSuggestion} from '../../models/user-suggestion';
import {map} from 'rxjs/operators';

@Injectable()
export class GhmListUserSuggestionService {
    constructor(@Inject(APP_CONFIG) private appConfig: IAppConfig,
                private http: HttpClient) {
    }

    search(isShowChildren: boolean, page = 1, pageSize = 5): Observable<SearchResultViewModel<UserSuggestion>> {
        const url = `${environment.apiGatewayUrl}api/v1/hr/users/user-children/${isShowChildren}`;
        return this.http.get(url, {
            params: new HttpParams()
                .set('page', page ? page.toString() : '')
                .set('pageSize', pageSize ? pageSize.toString() : '')
        }).pipe(map((result: SearchResultViewModel<UserSuggestion>) => {
            if (result.items) {
                result.items.forEach((item: UserSuggestion) => {
                    item.id = item.userId;
                });
            }
            return result;
        }))  as Observable<SearchResultViewModel<UserSuggestion>>;
    }
}
