import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {UserSuggestion} from '../../models/user-suggestion';
import {SearchResultViewModel} from '../../view-models/search-result.viewmodel';
import {GhmListUserSuggestionService} from './ghm-list-user-suggestion.service';
import {BaseListComponent} from '../../../base-list.component';

@Component({
    selector: 'ghm-list-user-suggestion',
    templateUrl: './ghm-list-user-suggestion.component.html',
    styleUrls: ['./ghm-list-user-suggestion.component.scss']
})

export class GhmListUserSuggestionComponent extends BaseListComponent<UserSuggestion> implements OnInit, AfterViewInit {
    @Input() isShowChildren = true;
    @Input() pageSize = 5;
    @Input() isShowPaging = false;
    @Output() userSelect = new EventEmitter();

    listUser: UserSuggestion[];

    constructor(private ghmShowUserSuggestionService: GhmListUserSuggestionService) {
        super();

        this.currentUser = this.appService.currentUser;
    }

    ngOnInit() {
    }

    ngAfterViewInit() {
        this.ghmShowUserSuggestionService.search(this.isShowChildren, this.currentPage, this.pageSize)
            .subscribe((result: SearchResultViewModel<UserSuggestion>) => {
                this.listUser = result.items;
                this.totalRows = result.totalRows;
            });
    }

    selectUser(user: UserSuggestion) {
        this.userSelect.emit(user);
    }
}
