import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GhmSelectDatetimeComponent } from './ghm-select-datetime.component';

describe('GhmSelectDatetimeComponent', () => {
  let component: GhmSelectDatetimeComponent;
  let fixture: ComponentFixture<GhmSelectDatetimeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GhmSelectDatetimeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GhmSelectDatetimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
