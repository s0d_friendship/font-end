import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {GhmSelectDatetimeComponent} from './ghm-select-datetime.component';
import {GhmDialogModule} from '../ghm-dialog/ghm-dialog.module';
import {FormsModule} from '@angular/forms';
import {DxDateBoxModule} from 'devextreme-angular';

@NgModule({
    imports: [
        CommonModule, DxDateBoxModule, GhmDialogModule, FormsModule
    ],
    declarations: [GhmSelectDatetimeComponent],
    exports: [GhmSelectDatetimeComponent]
})
export class GhmSelectDatetimeModule {
}
