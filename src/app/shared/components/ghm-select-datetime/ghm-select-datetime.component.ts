import {Component, EventEmitter, Input, OnInit, Output, ViewChild, ViewEncapsulation} from '@angular/core';
import {DateRange} from '../../constants/date-range.const';
import * as moment from 'moment';
import {GhmDialogComponent} from '../ghm-dialog/ghm-dialog.component';
import * as _ from 'lodash';

export class DateRangeValue {
    constructor(public id: any,
                public name: string,
                public isSelected?: boolean) {
    }
}

@Component({
    selector: 'ghm-select-datetime',
    templateUrl: './ghm-select-datetime.component.html',
    styleUrls: ['./ghm-select-datetime.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class GhmSelectDatetimeComponent implements OnInit {
    @ViewChild('ghmSelectDatetime',  {static: false}) ghmSelectDate: GhmDialogComponent;
    @Input() icon = 'fa fa-calendar';
    @Input() elementId = 'ghmSelectDatetime';
    @Input() startDate;
    @Input() endDate;
    @Input() readonly = false;
    @Output() selectDate = new EventEmitter();
    label = 'Tất cả';
    title;
    listDateRange = [
        new DateRangeValue(DateRange.all, 'Tất cả', false),
        new DateRangeValue(DateRange.today, 'Hôm nay', false),
        new DateRangeValue(DateRange.recentOneWeek, 'Một tuần gần đây', false),
        new DateRangeValue(DateRange.thisWeek, 'Tuần này', false),
        new DateRangeValue(DateRange.lastWeek, 'Tuần trước', false),
        new DateRangeValue(DateRange.recentOneMonth, 'Một tháng gần đây', false),
        new DateRangeValue(DateRange.thisMonth, 'Tháng này', false),
        new DateRangeValue(DateRange.lastMonth, 'Tháng trước', false),
        new DateRangeValue(DateRange.recentQuarter, 'Một quý gần đây', false),
        new DateRangeValue(DateRange.thisQuarter, 'Quý này', false),
        new DateRangeValue(DateRange.lastQuarter, 'Quý trước', false),
        new DateRangeValue(DateRange.recentYear, 'Một năm gần đây', false),
        new DateRangeValue(DateRange.thisYear, 'Năm nay', false),
        new DateRangeValue(DateRange.lastYear, 'Năm ngoái', false),
    ];

    private _dateRange;
    @Input()
    set dateRange(val: DateRangeValue) {
        if (val != null) {
            this._dateRange = val;
            this.getSelectItem(val);
            this.selectDate.emit({
                startDate: this.startDate ? moment(this.startDate).format('YYYY/MM/DD HH:mm') : '',
                endDate: this.endDate ? moment(this.endDate).format('YYYY/MM/DD HH:mm') : ''
            });
        }
    }

    @Input()
    set setStartDate(val) {
        if (val) {
            this.title = `${moment(this.startDate).format('DD/MM/YYYY')} - ${moment(this.endDate).format('DD/MM/YYYY')}`;
            this.label = '';
        }
    }

    get dateRange() {
        return this._dateRange;
    }

    constructor() {
    }

    ngOnInit() {
    }

    selectItem(item) {
        if (item) {
            this.getSelectItem(item);
            this.selectDate.emit({
                startDate: this.startDate ? moment(this.startDate).format('YYYY/MM/DD HH:mm') : '',
                endDate: this.endDate ? moment(this.endDate).format('YYYY/MM/DD HH:mm') : ''
            });
            this.ghmSelectDate.dismiss();
        }
    }

    selectDateTime() {
        this.selectDate.emit({
            startDate: this.startDate ? moment(this.startDate).format('YYYY/MM/DD HH:mm') : '',
            endDate: this.endDate ? moment(this.endDate).format('YYYY/MM/DD HH:mm') : ''
        });
        this.title = `${this.startDate ? moment(this.startDate).format('DD/MM/YYYY') : ''}
         - ${this.endDate ? moment(this.endDate).format('DD/MM/YYYY') : ''}`;
        this.label = '';
        this.ghmSelectDate.dismiss();
    }

    private getSelectItem(value: DateRangeValue) {
        const dateRangeInfo = _.find(this.listDateRange, (dateRange: DateRangeValue) => {
            return dateRange.id === value.id;
        });

        if (dateRangeInfo !== null) {
            _.each(this.listDateRange, (dateRange: DateRangeValue) => {
                dateRange.isSelected = false;
            });

            dateRangeInfo.isSelected = true;
            this.convertDateRangeToDate(value.id);
            this.label = dateRangeInfo.name;
        }
    }

    private convertDateRangeToDate(dateRange) {
        switch (dateRange) {
            case DateRange.today:
                this.startDate = moment();
                this.endDate = moment();
                break;
            case DateRange.recentOneWeek:
                this.startDate = moment().subtract(1, 'weeks');
                this.endDate = moment();
                break;
            case  DateRange.thisWeek:
                this.startDate = moment().startOf('isoWeek');
                this.endDate = moment().endOf('isoWeek');
                break;
            case DateRange.lastWeek:
                this.startDate = moment().subtract(1, 'weeks').startOf('isoWeek');
                this.endDate = moment().subtract(1, 'weeks').endOf('isoWeek');
                break;
            case DateRange.recentOneMonth:
                this.startDate = moment().subtract(1, 'months');
                this.endDate = moment();
                break;
            case  DateRange.thisMonth:
                this.startDate = moment().startOf('month');
                this.endDate = moment().endOf('month');
                break;
            case DateRange.lastMonth:
                this.startDate = moment().subtract(1, 'months').startOf('month');
                this.endDate = moment().subtract(1, 'months').endOf('month');
                break;
            case DateRange.recentQuarter:
                this.startDate = moment().subtract(1, 'quarters');
                this.endDate = moment();
                break;
            case  DateRange.thisQuarter:
                this.startDate = moment().startOf('quarter');
                this.endDate = moment().endOf('quarter');
                break;
            case DateRange.lastQuarter:
                this.startDate = moment().subtract(1, 'quarters').startOf('quarter');
                this.endDate = moment().subtract(1, 'quarters').endOf('quarter');
                break;
            case DateRange.recentYear:
                this.startDate = moment().subtract(1, 'years');
                this.endDate = moment();
                break;
            case  DateRange.thisYear:
                this.startDate = moment().startOf('year');
                this.endDate = moment().endOf('year');
                break;
            case DateRange.lastYear:
                this.startDate = moment().subtract(1, 'years').startOf('year');
                this.endDate = moment().subtract(1, 'years').endOf('year');
                break;
            case DateRange.all:
                this.startDate = '';
                this.endDate = '';
                break;
            default:
                this.startDate = '';
                this.endDate = '';
        }
    }
}
