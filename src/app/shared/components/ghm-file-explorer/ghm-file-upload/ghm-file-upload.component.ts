import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {GhmFileUploadService} from './ghm-file-upload.service';
import {ActionResultViewModel} from '../../../view-models/action-result.viewmodel';

@Component({
    selector: 'ghm-file-upload',
    templateUrl: './ghm-file-upload.component.html',
    encapsulation: ViewEncapsulation.None
})
export class GhmFileUploadComponent implements OnInit, AfterViewInit {
    @Input() folderId;
    @Input() multiple = true;
    @Input() text = 'Choose file';
    @Input() isTrash = true;
    @Input() isPublic = false;
    @Output() uploaded = new EventEmitter();

    @Input() uploading = false;
    percentUpload;

    constructor(private ghmFileUploadService: GhmFileUploadService) {
    }

    ngOnInit() {
    }

    ngAfterViewInit() {
        this.ghmFileUploadService.sent$.subscribe(result => this.uploading = true);
        this.ghmFileUploadService.progress$.subscribe(result => {
            this.percentUpload = result;
        });
        this.ghmFileUploadService.complete$.subscribe(result => {
            this.uploading = false;
        });
    }

    onFileChange(event: any) {
        const files = event.target.files;
        this.ghmFileUploadService.upload(files, {folderId: this.folderId, isTrash: this.isTrash, isPublic: this.isPublic})
            .subscribe((response: ActionResultViewModel<any>) => {
                if (response.code > 0) {
                    this.uploaded.next(response.data);
                }
            });
    }

}
