export class NhSuggestion<T = any> {
    id: string | number;
    icon: string;
    name: string;
    isSelected: boolean;
    isActive: boolean;
    data: T;
    description: string;
    image: string;

    constructor(id?: string | number, name?: string, icon?: string, isSelected?: boolean, isActive?: boolean, data?: any) {
        this.id = id;
        this.name = name;
        this.icon = icon;
        this.isSelected = isSelected !== undefined && isSelected != null ? isSelected : false;
        this.isActive = isActive !== undefined && isActive != null ? isActive : false;
        this.data = data;
        this.description = '';
        this.image = null;
    }
}
