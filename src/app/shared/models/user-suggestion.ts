export class UserSuggestion {
    id: string;
    userId: string;
    fullName: string;
    officeName: string;
    positionName: string;
    avatar: string;
    isSelected: boolean;
    isActive: boolean;

    constructor(id?: string, fullName?: string, officeName?: string, positionName?: string, avatar?: string, isSelected?: boolean) {
        this.id = id;
        this.fullName = fullName;
        this.officeName = officeName;
        this.positionName = positionName;
        this.avatar = avatar;
        this.isSelected = isSelected;
        this.isActive = false;
    }
}
