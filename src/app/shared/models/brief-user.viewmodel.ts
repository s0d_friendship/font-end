export class BriefUser {
    id: string;
    tenantId: string;
    userName: string;
    fullName: string;
    avatar: string;
    description: string;
}
