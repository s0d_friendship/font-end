// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {CoreModule} from '../../layout/core/core.module';
import {PartialsModule} from '../../partials/partials.module';
// Partials
// Pages

@NgModule({
  declarations: [],
  exports: [],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    CoreModule,
    PartialsModule,
  ],
  providers: []
})
export class PagesModule {
}
