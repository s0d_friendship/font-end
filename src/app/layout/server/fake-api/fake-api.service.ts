// Angular
import { Injectable } from '@angular/core';
// Angular in memory
import { InMemoryDbService } from 'angular-in-memory-web-api';
// RxJS
import { Observable } from 'rxjs';
// Auth// ECommerce
// Models
import { CarsDb } from './fake-db/cars';
import {AuthDataContext} from '../../../auth';

@Injectable()
export class FakeApiService implements InMemoryDbService {
  private AuthataContext: any;
  /**
   * Service Constructors
   */
  constructor() {
  }

  /**
   * Create Fake DB and API
   */
  createDb(): {} | Observable<{}> {
    // tslint:disable-next-line:class-name
    const db = {
      // auth module
      users: this.AuthataContext.users,
      roles: AuthDataContext.roles,
      permissions: AuthDataContext.permissions,

      // e-commerce
      // customers

      // orders
      // data-table
      cars: CarsDb.cars
    };
    return db;
  }
}
