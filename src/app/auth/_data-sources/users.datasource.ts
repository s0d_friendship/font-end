// NGRX
import { Store, select } from '@ngrx/store';
// CRUD
// State
import { selectUsersInStore, selectUsersPageLoading, selectUsersShowInitWaitingMessage } from '../_selectors/user.selectors';
import {AppState} from '../../layout/core/reducers';
import {BaseDataSource} from '../../shared/models/_base.datasource';
import {QueryResultsModel} from '../../shared/models/query-models/query-results.model';



export class UsersDataSource extends BaseDataSource {
  constructor(private store: Store<AppState>) {
    super();

    this.loading$ = this.store.pipe(
      select(selectUsersPageLoading)
    );

    this.isPreloadTextViewed$ = this.store.pipe(
      select(selectUsersShowInitWaitingMessage)
    );

    this.store.pipe(
      select(selectUsersInStore)
    ).subscribe((response: QueryResultsModel) => {
      this.paginatorTotalSubject.next(response.totalCount);
      this.entitySubject.next(response.items);
    });
  }
}
