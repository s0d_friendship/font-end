export class PageConfig {
  public defaults: any = {
    dashboard: {
      page: {
        title: 'Dashboard',
        desc: 'Latest updates and statistic charts'
      },
    },
    builder: {
      page: {title: 'Layout Builder', desc: ''}
    },
    header: {
      actions: {
        page: {title: 'Actions', desc: 'Actions example page'}
      }
    },
  };

  public get configs(): any {
    return this.defaults;
  }
}

